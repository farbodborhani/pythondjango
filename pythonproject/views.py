from django.shortcuts import render,redirect


def home(request):
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
    else:
        text="Login"
    print(text)    
    return render(request,'home.htm',{'text':text})
