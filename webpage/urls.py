
from django.urls import path
from . import views
app_name='webpage'
urlpatterns = [
    path('about_us/',views.about,name='about'),
    path('gallery/',views.gallery,name='gallery'),
    path('service/',views.service,name='service'),
    path('features/',views.features,name='features'),
    path('login/',views.login_page,name='login_page'),
    path('register/',views.signup_page,name='signup_page'),
    path('detail/',views.detail,name='detail'),
    path('logout/',views.logout_page,name="logout"),
]
