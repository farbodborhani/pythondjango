from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth import login,logout


def about(request):
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
    else:
        text='Login'
    return render(request,'webpage/about-us.htm',{'text':text})

def gallery(request):
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
    else:
        text='Login'
    return render(request,'webpage/gallery.htm',{'text':text})


def service(request):
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
    else:
        text='Login'
    return render(request,'webpage/services.htm',{'text':text})


def features(request):
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
    else:
        text='Login'
    return render(request,'webpage/components.htm',{'text':text})


def login_page(request):
    form=AuthenticationForm()
    if(request.method=='POST'):
        form= AuthenticationForm(data=request.POST)
        if(form.is_valid()):
            user=form.get_user()
            login(request,user)
            return redirect('webpage:detail')

    return render(request,'webpage/login.html',{'form':form})

def signup_page(request):
    form= UserCreationForm()
    if(request.method=='POST'):
        form= UserCreationForm(request.POST)
        if(form.is_valid()):
            user=form.save()
            login(request,user)
            return redirect('webpage:detail')
    return render(request,'webpage/signup.html',{'form':form})

def detail(request):
    x=request.user.is_authenticated
    print(x)
    text=''
    if(request.user.is_authenticated==True):
        text='Logout'
        return render(request,'webpage/blog-detail.htm',{'text':text})
    else:
        text='Login'
    return render(request,'home.htm',{'text':text})


# def logout_page(request):
#
#     if(request.method=='POST'):
#         logout(request)
#     return render(request,'home.htm')

def logout_page(request):
    text=''
    if(request.user.is_authenticated==True):
        logout(request)
        text='Login'
        return render(request,'home.htm',{'text':text})
    elif(request.user.is_authenticated==False):
        text='Logout'
        return redirect('webpage:login_page')
