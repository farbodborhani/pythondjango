from django.apps import AppConfig


class TemplateshapeConfig(AppConfig):
    name = 'templateshape'
